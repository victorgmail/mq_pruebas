#line 1 "c:\\Users\\57321\\OneDrive\\Desktop\\Opcion\\Programacion_sensores\\mq_pruebas\\calibracion.cpp"
#include <Arduino.h>

float* MQcalibration(const int puerto_lectura[10], float factor_aire_limpio[10], int muestras, float RL[10]){
    float suma[] = {0,0,0,0,0,0,0,0,0,0};
    float promedio[] = {0,0,0,0,0,0,0,0,0,0};
    static float RO[] = {0,0,0,0,0,0,0,0,0,0};
    for(int i=0; i<=muestras; i++){
        for(int j=0; j<=9; j++){
            suma[j]+=(((1023-(float)analogRead(puerto_lectura[j]))/(float)analogRead(puerto_lectura[j]))*RL[j]);
            Serial.print("suma[");
            Serial.print(j);
            Serial.print("]: ");
            Serial.print(suma[j]);
            Serial.print("  ");
        }
        Serial.print("\n");
        delay(500);
    }
    for (int j = 0; j <= 9; j++)
    {
        promedio[j]=suma[j]/muestras;
        Serial.print("promedio[");
        Serial.print(j);
        Serial.print("]: ");
        Serial.print(promedio[j]);
        Serial.print("  ");
    }
    Serial.print("\n");
    for (int j= 0; j <= 9; j++){
        RO[j] = promedio[j]/factor_aire_limpio[j];
        Serial.print("Ro[");
        Serial.print(j);
        Serial.print("]: ");
        Serial.print(RO[j]);
        Serial.print(" ");
    }
    Serial.print("\n");
    return RO;
}
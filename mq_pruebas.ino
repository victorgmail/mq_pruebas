// Programacion E nose - calibracion de sensores de sensores de la familia MQ. 
// temperatura 26 °C ambiente calculado por DHT 11
// Humedad de  27%  ambiente calculado por DHT 11
#include "calibracion.h"

const int MQ_PIN[]={A0,A1,A2,A3,A4,A5,A6,A7,A8,A9};
float Lectura[9];
float Razon_medicion_MQ[9];
float MM_MQ [9];
float Med_PPM_FIL[9];
float MQ_L[10];
float f[] = {59.099, 25.1, 6.44, 3.635, 9.88, 59.099, 25.1, 6.44, 3.635, 9.88};
float* Ro;;

// MQ3 - ALCOHOL
float RL_MQ3_ALC = 991; 
// MQ7 - CO
float RL_MQ7_CO = 950;
// MQ5 - DIHIDROGENO
float RL_MQ5_H2 = 985;
// MQ135 - ACETONA
float RL_MQ135_ACE = 905; 
// MQ6 - CH4
float RL_MQ6_CH4 = 982; 
// matriz de resistencias

float RL_MQ[]={RL_MQ3_ALC,RL_MQ7_CO,RL_MQ5_H2,RL_MQ135_ACE,RL_MQ6_CH4,RL_MQ3_ALC,RL_MQ7_CO,RL_MQ5_H2,RL_MQ135_ACE,RL_MQ6_CH4};
float RL_L[10];
//Modelos matematicos mx^b
float Matriz_m[] = {297.57,93.076,1226.7,35.012,2205,297.57,93.076,1226.7,35.012,2205};
float Matriz_b[] = {-1.481,-1.534,-4.257,-3.378,-2.507,-1.481,-1.534,-4.257,-3.378,-2.507};
// Filtrado de datos de datos 
#define alpha_MQ 0.05

void setup() {
  Serial.begin(9600);
  Ro=MQcalibration(MQ_PIN, f, 10, RL_MQ);
}
void loop() {
 for (int r = 0; r <= 9; r++){
   Razon_medicion_MQ[r] = (((1023-(float)analogRead(MQ_PIN[r]))/(float)analogRead(MQ_PIN[r]))*RL_MQ[r])/Ro[r];
   MM_MQ[r] = pow(Matriz_m[r]*Razon_medicion_MQ[r],Matriz_b[r]);
   Med_PPM_FIL[r] = (alpha_MQ*MM_MQ[r])+((1-alpha_MQ)*MM_MQ[r]);
   Lectura[r] = Med_PPM_FIL[r];
   Serial.print(Lectura[r]);
   Serial.print(",");
   delay(100);
 }

  for(int i=0; i<=9; i++){
    Serial.print(Ro[i]);
    Serial.print(",");
  }
  Serial.print("\n");
  delay(5000);

//for (int r = 0; r <= 10; r++){
//  float Razon_medicion_MQ = (((1023-(float)analogRead(MQ_PIN[r]))/(float)analogRead(MQ_PIN[r]))*RL_MQ[r])/RO;
//  float MM_MQ = Matriz_m[r]*Razon_medicion_MQ^(Matriz_b[r]);
//  float Med_PPM_FIL = (alpha_MQ*MM_MQ)+((1-alpha_MQ)*MM_MQ);
//}


//  for(int L = 0; L < 8; L++){
//    lectura[L]=(float)analogRead(MQ_PIN[L]);
//    Serial.print(lectura[L]);
//  }
//  lectura[9]=(float)analogRead(MQ_PIN[9]);
//  Serial.print(",");
//  delay(100);
}